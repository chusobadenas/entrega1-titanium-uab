function Utils() {

  this.isAndroid = function() {
    return (Ti.Platform.name == 'android');
  };

  this.isMobileWeb = function() {
    return (Ti.Platform.name == 'mobileweb');
  };

  this.isIOS = function() {
    return (Ti.Platform.name == 'iPhone OS');
  };

  this.isTizen = function() {
    return (Ti.Platform.name == 'TIZEN');
  };
}

module.exports = Utils;
