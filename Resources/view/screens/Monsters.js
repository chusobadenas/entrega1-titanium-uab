function Monsters() {
  this.box = null;

  this.build = function() {
    var box = Ti.UI.createView();

    var Header = require('view/components/Header');
    var header = new Header('Choose a Monster', true);
    box.add(header.box);

    var monsterData = require('data/monster_data').get();
    var ElementList = require('view/components/ElementList');
    var el = new ElementList(monsterData);
    box.add(el.box);

    // Save view
    this.box = box;
  };

  this.build();
}

module.exports = Monsters;
