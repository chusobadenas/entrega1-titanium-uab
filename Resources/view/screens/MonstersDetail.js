function MonstersDetail(monsterData) {
  this.detailWin = null;

  this.build = function() {
    var detailWin = Ti.UI.createWindow({
      title : 'Monster Detail',
      backgroundImage : 'images/background.jpg'
    });

    var box = Ti.UI.createView();

    // HEADER
    var Header = require('view/components/Header');
    var header = new Header(monsterData.name, false);
    box.add(header.box);

    // MONSTER DETAIL
    var Info = require('view/components/Info');
    var info = new Info(monsterData);
    box.add(info.box);

    // CLOSE BUTTON
    var closeButton = Ti.UI.createButton({
      title : 'Close',
      bottom : '10%'
    });

    closeButton.addEventListener('click', function() {
      detailWin.close();
      detailWin = null;
    });

    // Add view to window
    detailWin.add(box);

    // For Android, the close button is not necessary, because we have the back button
    if (!utils.isAndroid()) {
      detailWin.add(closeButton);
    }

    // Save window
    this.detailWin = detailWin;
  };

  this.show = function() {
    this.detailWin.open();
  };

  this.build();
}

module.exports = MonstersDetail;
