function Header(title, showImage) {
  this.box = null;

  this.build = function() {
    var header = Ti.UI.createLabel({
      width : '100%',
      height : 100,
      top : 0,
      text : title,
      color : '#fff',
      shadowColor : '#444',
      shadowOffset : {
        x : 1,
        y : 1
      },
      font : {
        fontSize : defaultFontSize + 15,
        fontWeight : 'bold',
      },
      textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER
    });
    
    if(showImage) {
      header.backgroundImage = 'images/headerbg.png';
      header.backgroundRepeat = true;
    }
    
    // Save view
    this.box = header;
  };

  this.build();
}

module.exports = Header;
