function ElementList(data) {
  this.box = null;

  this.build = function() {
    var tableData = [];
    var oddRow = false;

    for (var i = 0, len = data.length; i < len; i++) {
      // Get row item
      var item = data[i];

      // Create row
      var row = Ti.UI.createTableViewRow({
        heigth : 100,
        backgroundColor : oddRow ? '#FFF' : '#EEE',
        monster : item
      });

      // Elements in row
      row.add(Ti.UI.createImageView({
        image : item.image,
        left : 0,
        top : 5,
        bottom : 5,
        width : 80,
        height : 90
      }));

      row.add(Ti.UI.createLabel({
        color : '#576996',
        font : {
          fontSize : 32,
          fontWeight : 'bold'
        },
        text : item.name,
        left : 100,
        top : 10
      }));

      var pointsTxt = '' + item.exp + ' points of experience';
      row.add(Ti.UI.createLabel({
        color : '#000',
        font : {
          fontSize : defaultFontSize,
          fontWeight : 'normal'
        },
        text : pointsTxt,
        left : 100,
        top : 60
      }));

      // Insert row
      tableData.push(row);

      // Change row
      oddRow = !oddRow;
    }

    var tableView = Ti.UI.createTableView({
      data : tableData,
      top : 100,
      right : 10,
      bottom : 10,
      left : 10
    });

    // Navigation
    tableView.addEventListener('touchstart', function(e) {
      var MonstersDetail = require('view/screens/MonstersDetail');
      var mDetail = new MonstersDetail(e.row.monster);
      mDetail.show();
    });

    // Save view
    this.box = tableView;
  };

  this.build();
}

module.exports = ElementList;
