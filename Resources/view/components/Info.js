function Info(monsterData) {
  this.box = null;

  this.createView = function(orientation) {
    return Ti.UI.createView({
      height : Ti.UI.SIZE,
      layout : orientation
    });
  };

  this.createLabel = function(text) {
    return Ti.UI.createLabel({
      color : '#000',
      font : {
        fontSize : defaultFontSize,
        fontWeight : 'normal'
      },
      height : Ti.UI.SIZE,
      text : text,
      left : 15,
      top : 5,
      bottom : 5
    });
  };

  this.createRow = function() {
    return Ti.UI.createTableViewRow({
      heigth : 'auto',
      backgroundColor : 'transparent'
    });
  };

  this.insertRow = function(tableData, view) {
    var row = this.createRow();
    row.add(view);

    tableData.push(row);
  };

  this.insertRowInSection = function(tableData, view, sectionTitle) {
    var sectionView = this.createView('vertical');
    sectionView.height = 34;

    var label = this.createLabel(sectionTitle);
    label.font = {
      fontSize : defaultFontSize + 2,
      fontWeight : 'bold'
    };

    sectionView.add(label);

    var section = Ti.UI.createTableViewSection({
      headerView : sectionView
    });

    var row = this.createRow();
    row.add(view);
    section.add(row);

    tableData.push(section);
  };

  this.build = function() {
    var tableData = [];

    /*** DESCRIPTION ***/
    var view = this.createView('horizontal');

    view.add(Ti.UI.createImageView({
      image : monsterData.image,
      top : 5,
      bottom : 5,
      width : 80,
      height : 90
    }));

    view.add(Ti.UI.createLabel({
      color : '#000',
      font : {
        fontSize : defaultFontSize,
        fontWeight : 'normal'
      },
      text : monsterData.description,
      top : 5,
      width : utils.isAndroid() ? 240 : 'auto'
    }));

    // Insert row
    this.insertRowInSection(tableData, view, 'Description');

    /*** CATEGORY ***/
    view = this.createView('vertical');
    view.add(this.createLabel(monsterData.category));

    // Insert row
    this.insertRowInSection(tableData, view, 'Category');

    /*** EXPERIENCE ***/
    view = this.createView('vertical');
    view.add(this.createLabel('' + monsterData.exp + ' points of experience'));

    // Insert row
    this.insertRowInSection(tableData, view, 'Experience');

    /*** SKILLS ***/
    view = this.createView('vertical');
    // Only for the last row
    view.height = 'auto';
    view.add(this.createLabel(monsterData.skill));

    // Insert row
    this.insertRowInSection(tableData, view, 'Skill');

    // Save view
    this.box = Ti.UI.createTableView({
      backgroundColor : '#99D4FAB7',
      borderRadius : 10,
      data : tableData,
      top : 100,
      right : 10,
      bottom : 10,
      left : 10,
      separatorColor : 'transparent',
      separatorStyle : 'none',
      selectionStyle : 'none',
      scrollable : false
    });
  };

  this.build();
}

module.exports = Info;
