function getData() {
  return [{
    name : 'Acidseeker',
    exp : 42,
    image : '/images/1.png',
    description : 'This monster has a second stomach which only stores acid. It also has a wide viewing angle thanks to his three eyes.',
    category : 'Inside closet',
    skill : 'Acid ejecting'
  }, {
    name : 'Venompest',
    exp : 123,
    image : '/images/2.png',
    description : 'This monster is able to generate the world\'s most dangerous poison. Be careful not to get too close to him.',
    category : 'Under bed',
    skill : 'Poison blast'
  }, {
    name : 'Wildteeth',
    exp : 76,
    image : '/images/3.png',
    description : 'This monster doesn\'t have passion with children. He hides in the bushes of the deepest forests.',
    category : 'Forest',
    skill : 'Eat children'
  }, {
    name : 'Dawnspawn',
    exp : 82,
    image : '/images/4.png',
    description : 'We can find him in the ocean. With its ray is able to freeze 1 kilometer around.',
    category : 'Ocean',
    skill : 'Frost ray'
  }, {
    name : 'Slugsnake',
    exp : 111,
    image : '/images/5.png',
    description : 'He hides in the top of the trees. He is capable of producing hundreds of poisonous spores in few seconds.',
    category : 'Forest',
    skill : 'Death spores'
  }, {
    name : 'Manymem',
    exp : 234,
    image : '/images/6.png',
    description : 'With his long arms he can extract brains easily.',
    category : 'Forest',
    skill : 'Brain extracting'
  }, {
    name : 'Gallhag',
    exp : 16,
    image : '/images/7.png',
    description : 'Thanks to her power to become invisible, is very difficult to see. Watch out, always attack from behind.',
    category : 'Inside closet',
    skill : 'Invisibility'
  }, {
    name : 'Stinkfreak',
    exp : 99,
    image : '/images/8.png',
    description : 'This monster is able to store in its huge belly large quantities of poisonous gas. He is recognized for his annoying noises when walking.',
    category : 'Under bed',
    skill : 'Gas breathing'
  }];
}

exports.get = getData;
