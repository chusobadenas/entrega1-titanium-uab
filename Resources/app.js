var Utils = require('view/util/Utils');
var utils = new Utils();

var defaultFontSize = utils.isAndroid() ? 16 : 14;

var win = Ti.UI.createWindow({
  title : 'TableView Demo',
  backgroundColor : '#0CF',
});

var Monsters = require('view/screens/Monsters');
var monsters = new Monsters();

win.add(monsters.box);
win.open();
